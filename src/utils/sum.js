export const sum = (a, b) => {
  if (!a || !b) {
    return 0;
  }
  return a + b;
};

export const sumArea = (val) => {
  return `พื้นที่ ${val}`;
};
