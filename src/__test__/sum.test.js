import { sum, sumArea } from "../utils/sum";

test("adds 1 + 2 to equal 3", () => {
  expect(sum(1, 2)).toBe(3);
});

test("adds 1 + 2 to Notequal", () => {
  expect(sum(1, 2)).not.toBe(4);
});

test("adds sumArea", () => {
  const value = 5000;
  const expectValue = `พื้นที่ ${value}`;
  expect(sumArea(value)).toEqual(expectValue);
});

describe("Test sum function sum value", () => {
  test("add value sum", () => {
    expect(sum(1, 2)).toBe(3);
  });
});

describe("Test sum function not value", () => {
  test("not value", () => {
    expect(sum(1)).toBe(0);
  });
});
