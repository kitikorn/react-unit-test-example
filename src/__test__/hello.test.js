import Hello from "../components/Hello";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe("Test Hello Component Render", () => {
  test("test render", () => {
    const expectText = "ggwp";
    const component = <Hello title={expectText} />;
    const { container } = render(component);
    expect(container).toMatchSnapshot();
  });
});

describe("Test Hello Component", () => {
  test("test title", () => {
    const expectText = "ggwp";
    const { getByText } = render(<Hello title={expectText} />);
    expect(getByText(expectText)).toBeInTheDocument(expectText);
  });
});

describe("Test Hello Component wiht by id", () => {
  test("test title by Id", () => {
    const expectText = "ggwp";
    const { getByTestId } = render(<Hello title={expectText} />);
    expect(getByTestId("title")).toHaveTextContent(expectText);
  });
});

describe("Test Hello Component Function", () => {
  test("test count start", () => {
    const { getByTestId } = render(<Hello title={"Hello"} />);
    expect(getByTestId("count")).toHaveTextContent(0);
  });

  test("test count after click", () => {
    const { getByTestId } = render(<Hello title={"Hello"} />);
    const button = getByTestId("button");
    userEvent.click(button);
    expect(getByTestId("count")).toHaveTextContent(1);
  });

  test("test count after multiple click", () => {
    const expectCount = 5;
    const { getByTestId } = render(<Hello title={"Hello"} />);
    const button = getByTestId("button");
    for (let index = 0; index < expectCount; index++) {
      userEvent.click(button);
    }
    expect(getByTestId("count")).toHaveTextContent(expectCount);
  });
});
