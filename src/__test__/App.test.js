import App from "../App";
import { render } from "@testing-library/react";

describe("Test App", () => {
  test("Render App", () => {
    const { getByTestId } = render(<App />);
    const element = getByTestId("app");
    expect(element).toBeInTheDocument();
  });
});
