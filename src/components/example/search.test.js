import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import SearchBox, { Search } from "./search";

describe("SearchBox", () => {
  test("renders SearchBox component", () => {
    render(<SearchBox />);
    // fails
    // expect(screen.getByText("Search")).toBeInTheDocument();

    // succeeds
    expect(screen.getByText("Search:")).toBeInTheDocument();
  });

  test("renders SearchBox getByRole", () => {
    render(<SearchBox />);

    expect(screen.getByRole("textbox")).toBeInTheDocument();
  });

  test("renders SearchBox queryByText", () => {
    render(<SearchBox />);
    expect(screen.queryByText(/Searches for JavaScript/)).toBeNull();
  });

  test("renders SearchBox findBy", async () => {
    render(<SearchBox />);
    expect(screen.queryByText(/Signed in as/)).toBeNull();
    screen.debug();
    expect(await screen.findByText(/Signed in as/)).toBeInTheDocument();
    screen.debug();
  });

  test("renders SearchBox fireEvent", async () => {
    const textSearch = "GGWP";
    const regex = new RegExp(`Searches for ${textSearch}`);
    render(<SearchBox />);
    await screen.findByText(/Signed in as/);

    expect(screen.queryByText(regex)).toBeNull();

    fireEvent.change(screen.getByRole("textbox"), {
      target: { value: textSearch },
    });

    expect(screen.getByText(regex)).toBeInTheDocument();
  });

  test("renders SearchBox userEvent", async () => {
    const textSearch = "GGWP";
    const regex = new RegExp(`Searches for ${textSearch}`);
    render(<SearchBox />);
    await screen.findByText(/Signed in as/);

    expect(screen.queryByText(regex)).toBeNull();

    userEvent.type(screen.getByRole("textbox"), textSearch);

    expect(screen.getByText(regex)).toBeInTheDocument();
  });
});

describe("Search Function", () => {
  const textSearch = "JavaScript";
  test("calls the onChange callback handler", () => {
    const onChange = jest.fn();

    render(
      <Search value="" onChange={onChange}>
        Search:
      </Search>
    );

    fireEvent.change(screen.getByRole("textbox"), {
      target: { value: textSearch },
    });

    expect(onChange).toHaveBeenCalledTimes(1);
  });

  test("calls the onChange callback handler userEvent", async () => {
    const onChange = jest.fn();

    render(
      <Search value="" onChange={onChange}>
        Search:
      </Search>
    );

    userEvent.type(screen.getByRole("textbox"), textSearch);
    expect(onChange).toHaveBeenCalledTimes(10);
  });
});
