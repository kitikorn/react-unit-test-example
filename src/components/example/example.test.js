import { render, screen, fireEvent } from "@testing-library/react";

import FavoriteNumber from "./example";

describe("FavoriteNumber", () => {
  test("renders FavoriteNumber component", () => {
    render(<FavoriteNumber />);
    expect(screen.getByText("Favorite Number")).toBeInTheDocument();
  });

  test("renders FavoriteNumber show number", () => {
    render(<FavoriteNumber />);
    fireEvent.change(screen.getByRole("spinbutton"), {
      target: { value: 1 },
    });
    expect(screen.queryByText("The number is invalid")).toBeNull();
  });

  test("renders FavoriteNumber show number invalid", () => {
    render(<FavoriteNumber />);
    fireEvent.change(screen.getByRole("spinbutton"), {
      target: { value: "ggwp" },
    });
    expect(screen.getByText("The number is invalid")).toBeInTheDocument();
  });
});
