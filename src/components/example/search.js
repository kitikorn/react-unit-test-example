import { useState, useEffect } from "react";

const SearchBox = () => {
  const [search, setSearch] = useState("");
  const [user, setUser] = useState(null);

  const handleChange = (event) => {
    setSearch(event.target.value);
  };

  const getUser = () => {
    return Promise.resolve({ id: "1", name: "Robin" });
  };

  useEffect(() => {
    const loadUser = async () => {
      const user = await getUser();
      setUser(user);
    };
    loadUser();
  }, []);

  return (
    <div>
      {user ? <p>Signed in as {user.name}</p> : null}

      <Search value={search} onChange={handleChange}>
        Search:
      </Search>

      <p>Searches for {search ? search : "..."}</p>
    </div>
  );
};

export const Search = ({ value, onChange, children }) => {
  return (
    <div>
      <label htmlFor="search">{children}</label>
      <input id="search" type="text" value={value} onChange={onChange} />
    </div>
  );
};

export default SearchBox;
