import "./App.css";
import Hello from "./components/Hello";
import FavoriteNumber from "./components/example/example";

function App() {
  return (
    <div data-testid="app" className="App">
      <Hello title="Hello World" />
      {/* <FavoriteNumber /> */}
    </div>
  );
}

export default App;
